#!/usr/bin/env bash

# setup after stack is started

. ./env

if [ ! -f "CONFIGURED" ] ; then
    ADMIN_LOGIN="admin@example.com"
    ADMIN_ORG="Example Inc."
    ADMIN_PASSWORD="Xapi4Ilias"
    $CONTAINER_ENV exec "${APP_ID}-api" node cli/dist/server createSiteAdmin "${ADMIN_LOGIN}" "${ADMIN_ORG}" "${ADMIN_PASSWORD}"
    touch CONFIGURED
else
    echo  "${APP_ID} already configured, skipping setup..."
fi
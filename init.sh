#!/usr/bin/env bash

# init before stack is starting

. ./env

if [ "${CONTAINER_ENV}" == "podman" ] ; then
  podman-compose up -d "${APP}"mongo
fi

if [ "${CONTAINER_ENV}" == "docker" ] ; then
  docker-compose up -d "${APP}"mongo
fi
sleep 5